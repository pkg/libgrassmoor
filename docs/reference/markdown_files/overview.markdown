### Libgrassmoor Overview

[Mail the maintainers](mailto:libgrassmoor@apertis.org)

Libgrassmoor is a library responsible for providing media info and media playback functionalities.

#### Brief introduction

* libgrassmoor tracker:
	grassmoor tracker is an abstraction layer for tracker search engine which in turn uses W3C standards for RDF ontologies using Nepomuk with SPARQL to query and update the meta data.

	Metatracker apis provides access to media content and its meta-data in the local file system as well as for removable media. Metatracker APIs are very simple to use and makes media search easy by avoiding writing complex sparql queries.

* libgrassmoor av player:
	libgrassmoor-av-player backend is implemented as a GObject but uses clutter-gst playback interface, therefore it can be reused only in any clutter-based projects. This served as the backend libraries for the tinwell service.

	For audio playback it is better to use tinwell service because tinwell audio service takes care of audio management, continuous playback and other playback funtionalities.

	For video playback, av player APIs should be used and audio management needs to be handled by the applications by using canterbury audio manager.

* libgrassmoor audio recorder:
	The audio recorder library is implemented as a GObject and uses gstreamer, therefore it can be reused in any framework that uses gstreamer as the multimedia framework backend.

	This served as the backend libraries for the audio service for recorder interface.Recording will for now only be supported in speex format.

	Currently audio recording is provided by tinwell service recorder interface because tinwell audio service takes care of the audio management also internally.

	Applications can use audio recorder APIs directly but in that case audio management needs to be handled by the applications by using canterbury audio manager service. 
