/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgrassmoor-audio-recorder.h>

typedef enum   _enAudioRecorderCodecType enAudioRecorderCodecType;
typedef enum   _enAudioRecorderProperty enAudioRecorderProperty; 
typedef enum   _enAudioRecorderSignals enAudioRecorderSignals;

struct _GrassmoorAudioRecorderPrivate
{
	gdouble recordingDuration;
	gint recorderCodecType;
	gboolean recordingState;
	gchar *recordingFilePath;
	GstElement *gSource;
	GstElement *mux;
	GstElement *speexEncoder;
	GstElement *fileSink;
	GstBus *recorderBus;
};

enum _enAudioRecorderCodecType
{
	CODEC_SPEEX, 
	CODEC_ERROR
};

enum _enAudioRecorderProperty
{
	PROP_FIRST,
	PROP_RECORDING,			
	PROP_RECORDING_FILE_PATH
};

enum _enAudioRecorderSignals
{
	RECORDER_ERROR,
	RECORDER_RESUMED,
	RECORDER_PAUSED,
	RECORDER_STARTED,
	RECORDER_COMPLETE,
	RECORDER_SIGNAL_LAST
};

static guint32 audio_recorder_signals[RECORDER_SIGNAL_LAST] = {0,};

G_DEFINE_TYPE (GrassmoorAudioRecorder, grassmoor_audio_recorder, GST_TYPE_PIPELINE);

#define GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(o)	  (G_TYPE_INSTANCE_GET_PRIVATE ((o), GRASSMOOR_TYPE_AUDIO_RECORDER, GrassmoorAudioRecorderPrivate))

static void initialise_recorder (GrassmoorAudioRecorder *audioRecorder, GstElement *speexEncoder, GstElement *mux, GstPad *linkPad, GstPad *muxPad);
static void grassmoor_audio_recorder_init (GrassmoorAudioRecorder *audioRecorder);
static gboolean recorder_bus_message_cb (GstBus  *bus, GstMessage *message, GrassmoorAudioRecorder *audioRecorder);
static void grassmoor_audio_recorder_class_init (GrassmoorAudioRecorderClass *klass);
static void grassmoor_audio_recorder_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec);
static void grassmoor_audio_recorder_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec);
static void grassmoor_audio_recorder_dispose (GObject *object);
static void grassmoor_audio_recorder_finalize (GObject *object);
static gboolean set_recording_file_path (GrassmoorAudioRecorder *self,
                                         const gchar *path);
static void set_recording_state (GrassmoorAudioRecorder *self,
                                 gboolean state);
static void get_recording_file_path(GObject *object, GValue *value);
static void get_recording_state (GObject *object, GValue *value);
static void change_recording_state(GrassmoorAudioRecorder *recorder, gboolean state);

static void grassmoor_audio_recorder_finalize (GObject *object)
{
	DEBUG ("entered");
	G_OBJECT_CLASS (grassmoor_audio_recorder_parent_class)->finalize (object);
}

static void grassmoor_audio_recorder_dispose (GObject *object)
{
	if(!GRASSMOOR_IS_AUDIO_RECORDER(object))
		return;
	DEBUG ("entered");

	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(object);

	if(GST_IS_ELEMENT(priv->fileSink))
	{
		gst_element_set_state( GST_ELEMENT(priv->fileSink), GST_STATE_NULL);
		priv->fileSink = NULL;
	}
	if(GST_IS_ELEMENT(priv->mux))
	{
		gst_element_set_state( GST_ELEMENT(priv->mux), GST_STATE_NULL);
		priv->mux = NULL;
	}
	if(GST_IS_ELEMENT(priv->speexEncoder))
	{
		gst_element_set_state( GST_ELEMENT(priv->speexEncoder), GST_STATE_NULL);
		priv->speexEncoder = NULL;
	}
	if(GST_IS_ELEMENT(priv->gSource))
	{
		gst_element_set_state( GST_ELEMENT(priv->gSource), GST_STATE_NULL);
		priv->gSource = NULL;
	}
	
	gst_element_set_state( GST_ELEMENT(object), GST_STATE_NULL);

	G_OBJECT_CLASS (grassmoor_audio_recorder_parent_class)->dispose (object);
}

static void grassmoor_audio_recorder_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
	GrassmoorAudioRecorder *self = GRASSMOOR_AUDIO_RECORDER (object);
	DEBUG ("entered");
	switch(propertyId)
	{
		case PROP_RECORDING:
                  set_recording_state (self, g_value_get_boolean (value));
			break;

		case PROP_RECORDING_FILE_PATH:
			set_recording_file_path (self, g_value_get_string (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_audio_recorder_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	DEBUG ("entered");
	switch(propertyId)
	{
		case PROP_RECORDING:
			get_recording_state(object, value);
			break;

		case PROP_RECORDING_FILE_PATH:
			get_recording_file_path(object, value);
			break;

			/* IN case the property is not installed for this object, throw
			 * an appropriate error */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_audio_recorder_class_init (GrassmoorAudioRecorderClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;
	GParamSpec *pspec = NULL;

	/* Assign pointers to our functions */
	o_class->finalize = grassmoor_audio_recorder_finalize;
	o_class->dispose = grassmoor_audio_recorder_dispose;
	o_class->set_property = grassmoor_audio_recorder_set_property;
	o_class->get_property = grassmoor_audio_recorder_get_property;

	/* Make the private structure a part of the class */
	g_type_class_add_private (klass, sizeof (GrassmoorAudioRecorderPrivate));

	 /**
      * GrassmoorAudioRecorder:recording:
      *
      * To set recording to play/pause state
      */
	pspec = g_param_spec_boolean("recording", "Recording", "State to indicate if media element's recorder is recording or is paused", FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(o_class, PROP_RECORDING, pspec);

	/**
     * GrassmoorAudioRecorder:record-file-location:
     *
     * file path to store recording data
     */
	pspec = g_param_spec_string("record-file-location", "Recorded-File-Location", "Path of the file where recorded data is to be stored", NULL, G_PARAM_READWRITE);
	g_object_class_install_property(o_class, PROP_RECORDING_FILE_PATH, pspec);

	/**
     * GrassmoorAudioRecorder::recorder-error:
     * @self: the #GrassmoorAudioRecorder that emitted the signal
 	 * @error: error message
     *
     * The ::recorder-error is emitted when any error occurs during recording
 	 *	
	 * Since: 1.0
     */
	audio_recorder_signals[RECORDER_ERROR] = g_signal_new ("recorder-error",
			G_TYPE_FROM_CLASS (o_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GrassmoorAudioRecorderClass, recorder_error),
			NULL, NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE, 1,
			G_TYPE_STRING, NULL);

	/**
     * GrassmoorAudioRecorder::recorder-completed:
     * @self: the #GrassmoorAudioRecorder that emitted the signal
     * @fileLoc: file path of recorded data set using @record-file-location property
     *
     * The ::recorder-completed is emitted on completion of recording.
	 *
	 * Since: 1.0
     */
	audio_recorder_signals[RECORDER_COMPLETE] = g_signal_new ("recorder-completed",
			G_TYPE_FROM_CLASS (o_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GrassmoorAudioRecorderClass, recorder_completed),
			NULL, NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE, 1,
			G_TYPE_STRING, NULL);

	 /**
      * GrassmoorAudioRecorder::recorder-started:
      * @self: the #GrassmoorAudioRecorder that emitted the signal 
      *
      * The ::recorder-started is emitted on start of audio recording.
	  *
	  * Since: 1.0
      */
	audio_recorder_signals[RECORDER_STARTED] = g_signal_new ("recorder-started",
			G_TYPE_FROM_CLASS (o_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GrassmoorAudioRecorderClass, recorder_started),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0,
			NULL);

	 /**
      * GrassmoorAudioRecorder::recorder-paused:
      * @self: the #GrassmoorAudioRecorder that emitted the signal
      *
      * The ::recorder-paused is emitted on pause of audio recording.
	  *
	  * Since: 1.0
      */
	audio_recorder_signals[RECORDER_PAUSED] = g_signal_new ("recorder-paused",
			G_TYPE_FROM_CLASS (o_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GrassmoorAudioRecorderClass, recorder_paused),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0,
			NULL);

	/**
     * GrassmoorAudioRecorder::recorder-resumed:
     * @self: the #GrassmoorAudioRecorder that emitted the signal
     *
     * The ::recorder-resumed is emitted on resume of audio recording.
	 *
	 * Since: 1.0
     */
	audio_recorder_signals[RECORDER_RESUMED] = g_signal_new ("recorder-resumed",
			G_TYPE_FROM_CLASS (o_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GrassmoorAudioRecorderClass, recorder_resumed),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0,
			NULL);
}

static void get_recording_state (GObject *object, GValue *value)
{
	if(!GRASSMOOR_IS_AUDIO_RECORDER(object))
		return;
	DEBUG ("entered");

	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(object); 

	if(NULL != priv)
		g_value_set_boolean(value, priv->recordingState);
}

static void get_recording_file_path(GObject *object, GValue *value)
{
	if(!GRASSMOOR_IS_AUDIO_RECORDER(object))
		return;
	DEBUG ("entered");

	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(object); 
	if(NULL != priv)
		g_value_set_string(value, priv->recordingFilePath);
}

static void change_recording_state(GrassmoorAudioRecorder *recorder, gboolean state)
{
	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(recorder); 
	DEBUG ("entered");
	if(NULL != priv->recordingFilePath)
	{
		if(TRUE == state)
		{
			gst_element_set_locked_state (priv->gSource, FALSE );
			gst_element_set_state(GST_ELEMENT(recorder), GST_STATE_PLAYING);
		}
		else
			gst_element_set_state (GST_ELEMENT(recorder), GST_STATE_PAUSED );
	}
}

static void
set_recording_state (GrassmoorAudioRecorder *self,
                     gboolean state)
{
  self->priv->recordingState = state;
  DEBUG ("entered");
  change_recording_state (self, state);
}

static gboolean
set_recording_file_path (GrassmoorAudioRecorder *self,
                         const gchar *path)
{
  if (path == NULL || g_strcmp0 (self->priv->recordingFilePath, path) == 0)
    return FALSE;

  g_free (self->priv->recordingFilePath);
  self->priv->recordingFilePath = g_strdup (path);

  g_object_set (G_OBJECT (self->priv->fileSink), "location", path, NULL);
  change_recording_state (self, self->priv->recordingState);

  return TRUE;
}

static gboolean recorder_bus_message_cb (GstBus  *bus, GstMessage *message, GrassmoorAudioRecorder *audioRecorder)
{
	gpointer src;
	src = GST_MESSAGE_SRC (message);
	DEBUG ("entered");
	if (GRASSMOOR_IS_AUDIO_RECORDER(src))
	{
		GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(audioRecorder);

		switch (GST_MESSAGE_TYPE (message)) 
		{
			/* End of recording */
			case GST_MESSAGE_EOS:
				{
					DEBUG ("GST_MESSAGE_EOS");
					if(TRUE == priv->recordingState)
						g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_COMPLETE], 0, priv->recordingFilePath, NULL); 		
					
					break;
				}

				/* Error Message */
			case GST_MESSAGE_ERROR:
				{
					gchar  *debug;
					GError *error;
					gst_message_parse_error (message, &error, &debug);
					g_free (debug);
					DEBUG ("GST_MESSAGE_ERROR: %s", error->message);
					g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_ERROR], 0, g_strdup(error->message), NULL); 		
					g_error_free (error);
					break;
				}

				/* State changed */
			case GST_MESSAGE_STATE_CHANGED:
				{
					DEBUG ("GST_MESSAGE_STATE_CHANGED");
					GstState oldState, newState;
					gst_message_parse_state_changed (message, &oldState, &newState, NULL);
					if (newState == GST_STATE_PAUSED)
					{
						if(oldState == GST_STATE_PLAYING)
							g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_PAUSED], 0, NULL);
						
						priv->recordingState = FALSE;
					}
					else if (newState == GST_STATE_PLAYING)
					{
						if (oldState == GST_STATE_PAUSED) 
						{
							if(FALSE == priv->recordingState)
								g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_STARTED], 0, NULL);
							else	
								g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_RESUMED], 0, NULL);
						}
						else if ( (oldState == GST_STATE_READY) || (oldState == GST_STATE_NULL) )
							g_signal_emit(audioRecorder, audio_recorder_signals[RECORDER_STARTED], 0, NULL);
						else
						{
							/* Do nothing */
						}
					}
					else
					{
						/* Do nothing for state changed to ready or null
						 * */
					}
					break;
				}

			default:
				break;
		}
	}
	
	return TRUE;
}

static void grassmoor_audio_recorder_init (GrassmoorAudioRecorder *audioRecorder)
{
	/* Get the private structure */ 
	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(audioRecorder); 
	if(NULL != priv)
	{
		/* Get the recorder bus and connect to all relevant signal types to
		 * recieve updates */
		priv->recorderBus = gst_pipeline_get_bus (GST_PIPELINE (audioRecorder));
		gst_bus_add_watch (priv->recorderBus, (GstBusFunc) recorder_bus_message_cb, audioRecorder);

		/* Initialise parameters */
		priv->recorderCodecType = CODEC_SPEEX;
		priv->recordingDuration = 0.0;
		priv->recordingState = FALSE;
	}

  audioRecorder->priv = priv;
}
			
static void initialise_recorder (GrassmoorAudioRecorder *audioRecorder, GstElement *speexEncoder, GstElement *mux, GstPad *linkPad, GstPad *muxPad)
{
	/* Get the private structure */ 
	GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(audioRecorder); 
	DEBUG ("entered");
	if(NULL != priv)
	{
		/* Create all the gst elements required for recording */
		priv->gSource = gst_element_factory_make ("pulsesrc", "mic-source");
		priv->speexEncoder = speexEncoder;
		priv->mux = mux;
		priv->fileSink = gst_element_factory_make ("filesink", "file-dest");

		/* Add all of them into the main pipeline */
		gst_bin_add_many (GST_BIN (audioRecorder), priv->gSource, priv->speexEncoder, priv->mux, priv->fileSink, NULL);

		/* we link all elements together (except the encoder to the mux) */
		gst_element_link_many (priv->gSource, priv->speexEncoder, NULL);

		/* Link the mux's muxpad with the speexencoder's src pad */
		gst_pad_link (linkPad, muxPad);
		gst_object_unref (GST_OBJECT (muxPad));

		/* Finally link the sink to the mux as we are now sure
		 * that the mux has a speex compatible pad */
		gst_element_link(priv->mux, priv->fileSink);
			
		gst_element_set_state (GST_ELEMENT(audioRecorder), GST_STATE_READY );
	}
}			

/**
 * grassmoor_audio_recorder_new:
 *
 * Creates new instance of #GrassmoorAudioRecorder.
 *
 * Returns: (transfer full): a #GrassmoorAudioRecorder
 **/
GrassmoorAudioRecorder *grassmoor_audio_recorder_new(void)
{
	GrassmoorAudioRecorder *recorder = NULL;

	/* First Check if there is a compatible pad in the mux element that
	 * can process speex Data */
	GstElement *speexEncoder = gst_element_factory_make ("speexenc", "speex-encoder" );
	GstElement *mux = gst_element_factory_make ("oggmux", "mux");
	GstPad *linkPad = gst_element_get_static_pad( speexEncoder, "src");
	GstPad *muxPad = gst_element_get_compatible_pad ( mux, linkPad, NULL);

	/* If the muxPad is not availble dont instantiate the recorder */
	if(NULL != muxPad)
	{
		/* Instantiate a new recorder */
		recorder = g_object_new(GRASSMOOR_TYPE_AUDIO_RECORDER, NULL);
	
		/* Initialise the recorder */
		if(GRASSMOOR_IS_AUDIO_RECORDER(recorder))
			initialise_recorder(recorder, speexEncoder, mux, linkPad, muxPad);
	}
	
	return recorder;
}

/**
 * grassmoor_audio_recorder_start_recording:
 * @self: a #GrassmoorAudioRecorder
 *
 * Start or resume a recording session.
 *
 */
void
grassmoor_audio_recorder_start_recording (GrassmoorAudioRecorder *self)
{
  g_return_if_fail (GRASSMOOR_IS_AUDIO_RECORDER (self));

  if (self->priv->recordingState)
    return;

  set_recording_state (self, TRUE);
  g_object_notify (G_OBJECT (self), "recording");
}

/**
 * grassmoor_audio_recorder_pause_recording:
 * @self: a #GrassmoorAudioRecorder
 *
 * Pause a recording session. Use grassmoor_audio_recorder_start_recording() to
 * resume recording.
 *
 */
void
grassmoor_audio_recorder_pause_recording (GrassmoorAudioRecorder *self)
{
  g_return_if_fail (GRASSMOOR_IS_AUDIO_RECORDER (self));

  if (!self->priv->recordingState)
    return;

  set_recording_state (self, FALSE);
  g_object_notify (G_OBJECT (self), "recording");
}

/**
 * grassmoor_audio_recorder_stop_recording:
 * @recorder: #GrassmoorAudioRecorder instance
 * 
 * this function is used to stop the audio recording 
 *
 * Returns: Recorder instance
 **/
void grassmoor_audio_recorder_stop_recording(GrassmoorAudioRecorder *recorder)
{
	if(GRASSMOOR_IS_AUDIO_RECORDER(recorder))
	{
		GrassmoorAudioRecorderPrivate *priv = GRASSMOOR_AUDIO_RECORDER_GET_PRIVATE(recorder);
		DEBUG ("entered");
		if(GST_STATE_CHANGE_SUCCESS == gst_element_set_state (GST_ELEMENT(recorder), GST_STATE_NULL ))
		{
			gst_element_set_state (priv->gSource, GST_STATE_NULL );
			gst_element_set_locked_state (priv->gSource, TRUE );
		}
	}
}

/**
 * grassmoor_audio_recorder_set_file_location:
 * @self: a #GrassmoorAudioRecorder
 * @path: a file path
 *
 * Set the #GrassmoorAudioRecorder:record-file-location
 *
 */
void
grassmoor_audio_recorder_set_file_location (GrassmoorAudioRecorder *self,
                                            const gchar *path)
{
  g_return_if_fail (GRASSMOOR_IS_AUDIO_RECORDER (self));
  g_return_if_fail (path);

  if (set_recording_file_path (self, path))
    g_object_notify (G_OBJECT (self), "record-file-location");
}
