/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

#include <glib.h>
#include <gst/gst.h>
#include <libgrassmoor-av-player.h>
#include <gio/gio.h>

#include "grassmoor-avplayer-enumtypes.h"

typedef enum _AVPlayerProperty AVPlayerProperty;
typedef struct _GrassmoorAVPlayerPrivate GrassmoorAVPlayerPrivate; 

enum _AVPlayerProperty
{
	PROP_AV_PLAYER_FIRST,
	PROP_AV_PLAYER_MODE,
	PROP_AV_PLAYER_ACTOR,
	PROP_AV_PLAYER_BUFFER_SIZE,
	PROP_AV_PLAYER_CAN_BUFFER
};

typedef enum _enAVPlayerSignals enAVPlayerSignals;
enum _enAVPlayerSignals
{
	/*< private >*/
	SIG_AV_PLAYER_VIDEO_ACTOR,
	SIG_AV_PLAYER_LAST_SIGNAL
};


struct _GrassmoorAVPlayerPrivate
{
	GstElement *pipeline;
	gint inPlayerMode;
	ClutterActor *pVideoActor;
	gboolean should_buffer_done;
	gboolean bHasHttp;
};

/**************** signal Flags *******************/
/* Storage for the signals */
static guint32 player_signals[SIG_AV_PLAYER_LAST_SIGNAL] = {0,};

G_DEFINE_TYPE_WITH_CODE (GrassmoorAVPlayer, grassmoor_av_player, CLUTTER_GST_TYPE_PLAYBACK,
                         G_ADD_PRIVATE (GrassmoorAVPlayer));


/************************************************************************************
 * Description of Fixes/Functionality:
-----------------------------------------------------------------------------------
        Description                             Date                    Name
        ----------                              ----                    ----
 ************************************************************************************/


/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/************************************************************************
 * Function : video_actor_destroy_cb
 * Description: on video actor destroy, free the content
 * Parameters: video actor, AVPlayer object as userdata
 * Return value:
 ************************************************************************/
static void video_actor_destroy_cb(ClutterActor *pActor, gpointer pUserData)
{
	DEBUG ("entered");
	//GrassmoorAVPlayerPrivate *priv = GRASSMOOR_AVPLAYER_GET_PRIVATE(GRASSMOOR_AV_PLAYER(pUserData));
	GObject *obj = NULL;
	g_object_get(pActor, "content", &obj, NULL);
	if(obj != NULL && G_IS_OBJECT(obj))
	{
		DEBUG ("unref content");
		g_object_unref(obj);
	}
}

static void video_actor_content_cb(GObject *pObject, GParamSpec *pspec, gpointer pUserData)
{
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (pUserData);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);
	DEBUG ("entered");

	if(priv->inPlayerMode == GRASSMOOR_AV_PLAYER_MODE_VIDEO && priv->pVideoActor)
	{
		GObject *obj = NULL;
		g_object_get(priv->pVideoActor, "content", &obj, NULL);
		if(obj != NULL && G_IS_OBJECT(obj))
		{
			DEBUG ("unref content");
			g_object_unref(obj);
		}
	}
}

# if 0
/************************************************************************
 * Function : check_player_mode
 * Description: checks if the player mode is valid
 * Parameters: AVPlayer object
 * Return value: returns current player mode
 ************************************************************************/
static gint check_player_mode(GObject *pObject)
{
	GrassmoorAVPlayerPrivate *priv = GRASSMOOR_AVPLAYER_GET_PRIVATE(GRASSMOOR_AV_PLAYER(pObject));
	if(priv->inPlayerMode == GRASSMOOR_AV_PLAYER_MODE_NONE)
		g_warning("Please the the player mode to audio/video\n");

	return priv->inPlayerMode;
}
# endif

/************************************************************************
 * Function : check_for_http_scheme
 * Description: check_for_http_scheme
 * Parameters: AVPlayer object
 * Return value: returns TRUE if uri has http scheme
 ************************************************************************/
static gboolean check_for_http_scheme (GrassmoorAVPlayer *avPlayer)
{
	gboolean hasHttp = FALSE;

	gchar *uri;
	g_object_get(avPlayer, "uri", &uri, NULL);

	if(NULL != uri)
	{
		GFile *pathFile = g_file_new_for_commandline_arg (uri);
		if (NULL != pathFile)
			hasHttp = g_file_has_uri_scheme (pathFile, "http");
	}

	return hasHttp;
}

/************************************************************************
 * Function : get_can_buffer_state
 * Description: whethre stream Cannot be buffered
 * Parameters: AVPlayer object
 * Return value: returns TRUE if stream can be buffered else FALSE
 ************************************************************************/
static gboolean get_can_buffer_state(GrassmoorAVPlayer *avPlayer)
{
	g_return_val_if_fail (GRASSMOOR_IS_AV_PLAYER (avPlayer), FALSE);

	gboolean canBuffer = FALSE;

	gboolean hasHttp = check_for_http_scheme(avPlayer);

	if(TRUE == hasHttp)
	{
		GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (avPlayer);
		GstState curState, pending;
		GstStateChangeReturn stateChange;

		/* Take a backup of the current state of the pipeline */
		gst_element_get_state(priv->pipeline, &curState, &pending, GST_CLOCK_TIME_NONE);  

		/* Change the state to ready */
		stateChange = gst_element_set_state(priv->pipeline, GST_STATE_READY);

		/* If the state change is successful, check if the stream can be
		 * buffered */
		if(GST_STATE_CHANGE_SUCCESS == stateChange)
		{
			/* Lock the pipeline to prevent changes on the parent bin to
			 * have any effect on change of state for the pipeline */
			gst_element_set_locked_state(priv->pipeline, TRUE);

			/* Change the state from ready to paused */ 
			stateChange = gst_element_set_state(priv->pipeline, GST_STATE_PAUSED);

			/* Based on the result of this state change set the can
			 * buffer property */
			switch (stateChange)
			{
			case GST_STATE_CHANGE_SUCCESS: 
			case GST_STATE_CHANGE_ASYNC: 
				canBuffer = TRUE;
				break;

			case GST_STATE_CHANGE_NO_PREROLL:
			case GST_STATE_CHANGE_FAILURE:
				canBuffer = FALSE;
				break;
			}

			/* Release the state change lock */
			gst_element_set_locked_state(priv->pipeline, FALSE);

			/* Set it back to the old state */
			gst_element_set_state(priv->pipeline, curState);
		}
	}

	return canBuffer;
}

/************************************************************************
 * Function : set_player_mode_video
 * Description: update the player mode to video and create video actor
 * Parameters: AVPlayer object
 * Return value: 
 ************************************************************************/
static void set_player_mode_video(GObject *object)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));

	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);
	DEBUG ("entered");

	if(priv->pVideoActor)
	{
		clutter_actor_destroy(priv->pVideoActor);
		priv->pVideoActor = NULL;
	}

	priv->pVideoActor = g_object_new (CLUTTER_TYPE_ACTOR,
			"content", g_object_new (CLUTTER_GST_TYPE_CROP/*_ASPECTRATIO*/, "player", object, /*"fill-allocation", TRUE,*/ NULL),
			NULL);

	//g_object_set(priv->pVideoActor, "content-gravity", CLUTTER_CONTENT_GRAVITY_RESIZE_ASPECT,NULL);
	/* connect the signal to know if actor gets destroyed from outside */
	g_signal_connect(priv->pVideoActor, "destroy", G_CALLBACK(video_actor_destroy_cb), object);	
	g_signal_connect(priv->pVideoActor, "notify::content", G_CALLBACK(video_actor_content_cb), object);	
	//g_signal_connect (object, "size-change", G_CALLBACK (size_change), object);

	/* emit the signal with created video actor to app */
	g_signal_emit_by_name(GRASSMOOR_AV_PLAYER(object), "video-actor-created", priv->pVideoActor); 
}

/************************************************************************
 * Function : set_player_mode_audio
 * Description: 
 * Parameters: AVPLayer onject
 * Return value: 
 ************************************************************************/
static void set_player_mode_audio(GObject *object)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));
	DEBUG ("entered");
	/* TBD............ may be detach video actor or some other actions */

	//GrassmoorAVPlayerPrivate *priv = GRASSMOOR_AVPLAYER_GET_PRIVATE(object);
}

static void
set_player_buffer_size (GrassmoorAVPlayer *self,
                        gint buffer_size)
{
  GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);

  if (!get_can_buffer_state (self))
    {
      WARNING ("Stream Cannot be buffered!");
      return;
    }

  g_object_set (priv->pipeline, "buffer-size", buffer_size, NULL);
}

/************************************************************************
 * Function : get_player_video_actor
 * Description: if player is in video made, returns the video actor
 * Parameters: AVPLayer object, GValue *
 * Return value: 
 ************************************************************************/
static void get_player_video_actor(GObject *object, GValue *value)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);

	if(priv->inPlayerMode == GRASSMOOR_AV_PLAYER_MODE_VIDEO)
		g_value_set_object(value, priv->pVideoActor);
	else
		WARNING ("video actor is not valid for player video mode");
}

/************************************************************************
 * Function : get_player_mode
 * Description: to get player mode either audio/video 
 * Parameters: AVPLayer object, GValue *
 * Return value: 
 *************************************************************************/
static void get_player_mode(GObject *object, GValue *value)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);
	g_value_set_int(value, priv->inPlayerMode);
}

/************************************************************************
 * Function : set_player_mode
 * Description: set player mode either audio/video
 * Parameters: AVPLayer object, GValue *
 * Return value: 
 ************************************************************************/
static void set_player_mode(GObject *object, const GValue *value)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);

	GrassmoorAVMode inMode = g_value_get_enum (value);
	//if(priv->inPlayerMode == inMode)
	//	return;
	priv->inPlayerMode = inMode;

	switch(inMode)
	{
	case GRASSMOOR_AV_PLAYER_MODE_VIDEO:
		set_player_mode_video(object);
		break;
	case GRASSMOOR_AV_PLAYER_MODE_AUDIO:
		set_player_mode_audio(object);
		break;
	default:
		WARNING ("Not a valid player mode : %d",inMode);
	}
}

static gint
get_player_buffer_size (GrassmoorAVPlayer *self)
{
  GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);
  gint buffer_size;

  if (!get_can_buffer_state (self))
    {
      WARNING ("Stream Cannot be buffered!");
      return 0;
    }

  g_object_get (priv->pipeline, "buffer-size", &buffer_size, NULL);
  return buffer_size;
}

static gboolean player_should_buffer_cb (GObject *object, GstQuery *query)
{
	g_return_val_if_fail (GRASSMOOR_IS_AV_PLAYER (object), FALSE);
	GrassmoorAVPlayer *avPlayer = GRASSMOOR_AV_PLAYER(object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (avPlayer);

	gdouble position;
	gdouble duration;
	gboolean ret = FALSE;
	gint64 left;
	gdouble time_left;
	gboolean busy;


	/* Use the estimated total duration left as estimated by queue2 based on the
	 * averge incoming bitrate, we can stop buffering once the remaining download
	 * takes less time then the remaining play time (with a 20% safety margin).
	 * However regardless of that keep buffering as long as queue2 indicates that
	 * buffering should happen (based on its high water marks */
	/*stops buffering when the remaining playback time is less then 120% of the remaining download time */
	gst_query_parse_buffering_range (query, NULL, NULL, NULL, &left);
	gst_query_parse_buffering_percent (query, &busy, NULL);

	position = clutter_gst_playback_get_position (CLUTTER_GST_PLAYBACK(avPlayer) );
	duration = clutter_gst_playback_get_duration (CLUTTER_GST_PLAYBACK(avPlayer) );
	DEBUG ("ms left = %" G_GINT64_FORMAT ", busy? = %d", left, busy);
	DEBUG (" position= %f duration = %f", position, duration);
	if (duration)
		time_left = duration - position;
	else
		time_left = 0;

	if (left == -1 || (!busy && (((gdouble)left * 1.2) / 1000) <= time_left)) 
	{
		ret = FALSE;
		priv->should_buffer_done = TRUE;
	} 
	else 
	{
		ret = TRUE;
	}

	DEBUG ("Application should buffer says: %d", ret);

	return ret;
}


static void on_notify_buffer_fill (GObject  *object, GParamSpec *pspec)
{
	gdouble buffer_fill;

	g_object_get (GRASSMOOR_AV_PLAYER(object), "buffer-fill", &buffer_fill, NULL);
	DEBUG ("Buffering - percentage=%d%%", (int) (buffer_fill * 100));
}

static void on_notify_set_uri(GObject  *object, GParamSpec *pspec)
{
	g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (object));
	GrassmoorAVPlayer *avPlayer = GRASSMOOR_AV_PLAYER(object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (avPlayer);

	gboolean hasHttp = check_for_http_scheme(avPlayer);

	if(TRUE == hasHttp && priv->bHasHttp == FALSE)
	{
		DEBUG ("Remote media detected, setting up buffering");

		clutter_gst_playback_set_buffering_mode (CLUTTER_GST_PLAYBACK(avPlayer), CLUTTER_GST_BUFFERING_MODE_DOWNLOAD);

		g_signal_connect (CLUTTER_GST_PLAYBACK(avPlayer),
				"notify::buffer-fill",
				G_CALLBACK (on_notify_buffer_fill),
				NULL);

		g_signal_connect (CLUTTER_GST_PLAYBACK(avPlayer),
				"should-buffer", 
				G_CALLBACK (player_should_buffer_cb),
				NULL);
		/* update flag to avoid signal reconnection */
		priv->bHasHttp = TRUE;
	}
	else if(FALSE == hasHttp)
	{
		DEBUG("disconnect buffer signals");
		priv->bHasHttp = FALSE;
		g_signal_handlers_disconnect_by_func(CLUTTER_GST_PLAYBACK(avPlayer), G_CALLBACK (on_notify_buffer_fill), NULL);
		g_signal_handlers_disconnect_by_func(CLUTTER_GST_PLAYBACK(avPlayer), G_CALLBACK (player_should_buffer_cb), NULL);
	}
}

/************************************************************************
 * Function : grassmoor_av_player_finalize
 * Description: Finalise the av player object
 * Parameters: The object reference
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_finalize (GObject *object)
{
	G_OBJECT_CLASS (grassmoor_av_player_parent_class)->finalize (object);
}

/************************************************************************
 * Function : grassmoor_av_player_dispose
 * Description: Dispose the av player object
 * Parameters: The object reference
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_dispose (GObject *object)
{
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);
	GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (self);

	if(priv->pVideoActor)
		clutter_actor_destroy(priv->pVideoActor);

	G_OBJECT_CLASS (grassmoor_av_player_parent_class)->dispose (object);
}

/************************************************************************
 * Function : grassmoor_av_player_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);

	switch(propertyId)
	{
	case PROP_AV_PLAYER_CAN_BUFFER:
		g_warning("Cannot Set A Property That Is Read Only! \n");
		break;

	case PROP_AV_PLAYER_BUFFER_SIZE:
		set_player_buffer_size (self, g_value_get_int (value));
		break;
	case PROP_AV_PLAYER_MODE:
		set_player_mode(object, value);
		break;
	case PROP_AV_PLAYER_ACTOR:
		g_warning("Cannot Set A Property That Is Read Only! \n");
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
		break;
	}
}

/************************************************************************
 * Function :   grassmoor_av_player_get_property
 * Description: get a property value
 * Parameters:  The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	GrassmoorAVPlayer *self = GRASSMOOR_AV_PLAYER (object);

	switch(propertyId)
	{
	case PROP_AV_PLAYER_CAN_BUFFER:
		g_value_set_boolean (value, get_can_buffer_state (self));
		break;

	case PROP_AV_PLAYER_BUFFER_SIZE:
		g_value_set_int (value, get_player_buffer_size (self));
		break;
	case PROP_AV_PLAYER_MODE:
		get_player_mode(object, value);
		break;	
	case PROP_AV_PLAYER_ACTOR:
		get_player_video_actor(object, value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
		break;
	}
}

/************************************************************************
 * Function : grassmoor_av_player_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_class_init (GrassmoorAVPlayerClass *klass)
{
GObjectClass *o_class = (GObjectClass *) klass;
GParamSpec *pspec = NULL;

/* Assign pointers to our functions */
o_class->finalize = grassmoor_av_player_finalize;
o_class->dispose = grassmoor_av_player_dispose;
o_class->get_property = grassmoor_av_player_get_property;
o_class->set_property = grassmoor_av_player_set_property;

/**
 * GrassmoorAVPlayer:can-buffer:
 *
 * State to indicate if buffering is possible on the media
 */
pspec = g_param_spec_boolean("can-buffer", 
		("Can-Buffer"), 
		("State to indicate if buffering is possible on the media"), 
		FALSE, 
		G_PARAM_READABLE);
g_object_class_install_property(o_class, PROP_AV_PLAYER_CAN_BUFFER, pspec);

/**
 * GrassmoorAVPlayer:buffer-size:
 *
 * To set the size of the buffer before play can start
 */
pspec = g_param_spec_int("buffer-size", 
		"Buffer-Size", 
		"Size Of the Buffer before which play can start",
		G_MININT, G_MAXINT, -1,
		G_PARAM_READWRITE);
g_object_class_install_property(o_class, PROP_AV_PLAYER_BUFFER_SIZE, pspec);

/**
 * GrassmoorAVPlayer:player-mode:
 *
 * property which has to be set at the time of creation of av player object.
 * Note:  This property decides whether video actor needs to be created if mode is set as video 
 */
  pspec = g_param_spec_enum ("player-mode", "Player-Mode",
                             "player is for audio mode or video mode",
                             GRASSMOOR_TYPE_AV_MODE,
                             GRASSMOOR_AV_PLAYER_MODE_INVALID,
                             G_PARAM_READWRITE);
g_object_class_install_property(o_class, PROP_AV_PLAYER_MODE, pspec);


/**
 * GrassmoorAVPlayer:video-actor:
 *
 * readable property to get video actor to show on ui.
 * Note:  This property is valid if av player mode is set to video. 
 */
pspec = g_param_spec_object("video-actor",
		"Video-Actor",
		"to get video actor if av player mode is set for video mode",
		CLUTTER_TYPE_ACTOR,
		G_PARAM_READABLE);
g_object_class_install_property(o_class, PROP_AV_PLAYER_ACTOR, pspec);

/**
 * GrassmoorAVPlayer:video-actor-created:
 *
 * ::video-actor-created is emitted when av player mode is set to video mode
 */	
player_signals[SIG_AV_PLAYER_VIDEO_ACTOR] = g_signal_new("video-actor-created",
		G_TYPE_FROM_CLASS (o_class),
		G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (GrassmoorAVPlayerClass, video_actor_created),
		NULL, NULL,
		g_cclosure_marshal_generic,
		G_TYPE_NONE, 1, CLUTTER_TYPE_ACTOR);


}

/************************************************************************
 * Function : grassmoor_av_player_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ************************************************************************/
static void grassmoor_av_player_init (GrassmoorAVPlayer *avPlayer)
{
	if(GRASSMOOR_IS_AV_PLAYER(avPlayer))
	{
		GrassmoorAVPlayerPrivate *priv = grassmoor_av_player_get_instance_private (avPlayer);

		/* initialise private variables */
		priv->pVideoActor = NULL;
		priv->should_buffer_done = FALSE;
		priv->bHasHttp = FALSE;
		/* default is audio mode */
		priv->inPlayerMode = GRASSMOOR_AV_PLAYER_MODE_AUDIO;

		priv->pipeline = clutter_gst_player_get_pipeline((ClutterGstPlayer *)avPlayer);

		/* By default ClutterGst seeks to the nearest key frame (faster). However
		 * it has the weird effect that when you click on the progress bar, the fill
		 * goes to the key frame position that can be quite far from where you
		 * clicked. Using the ACCURATE flag tells playbin2 to seek to the actual
		 * frame */
		clutter_gst_playback_set_seek_flags (CLUTTER_GST_PLAYBACK(avPlayer), CLUTTER_GST_SEEK_FLAG_ACCURATE);

		g_signal_connect (avPlayer, "notify::uri", G_CALLBACK (on_notify_set_uri), avPlayer);
	}
}

/**
 * grassmoor_av_player_new:
 * @mode: a #GrassmoorAVMode
 *
 * Creates a new #GrassmoorAVMode with @mode as GrassmoorAVMode:player-mode:.
 *
 * Returns: (transfer full): a #GrassmoorAVPlayer
 */
GrassmoorAVPlayer *
grassmoor_av_player_new (GrassmoorAVMode mode)
{
  g_return_val_if_fail (mode > GRASSMOOR_AV_PLAYER_MODE_INVALID, NULL);

  return g_object_new (GRASSMOOR_TYPE_AV_PLAYER,
                       "player-mode", mode,
                       NULL);
}

/**
 * grassmoor_av_player_get_video_actor:
 * @self: a #GrassmoorAVPlayer of mode %GRASSMOOR_AV_PLAYER_MODE_VIDEO
 *
 * The value of the GrassmoorAvPlayer:video-actor property.
 *
 * Returns: (transfer none): a #ClutterActor
 */
ClutterActor *
grassmoor_av_player_get_video_actor (GrassmoorAVPlayer *self)
{
  GrassmoorAVPlayerPrivate *priv;

  g_return_val_if_fail (GRASSMOOR_IS_AV_PLAYER (self), NULL);
  priv = grassmoor_av_player_get_instance_private (self);
  g_return_val_if_fail (priv->inPlayerMode == GRASSMOOR_AV_PLAYER_MODE_VIDEO, NULL);

  return priv->pVideoActor;
}

/**
 * grassmoor_av_player_get_can_buffer:
 * @self: a #GrassmoorAVPlayer
 *
 * Return the #GrassmoorAVPlayer:can-buffer property.
 *
 * Returns: the value of #GrassmoorAVPlayer:can-buffer property
 */
gboolean
grassmoor_av_player_get_can_buffer (GrassmoorAVPlayer *self)
{
  g_return_val_if_fail (GRASSMOOR_IS_AV_PLAYER (self), FALSE);

  return get_can_buffer_state (self);
}

/**
 * grassmoor_av_player_get_buffer_size:
 * @self: a #GrassmoorAVPlayer
 *
 * Return the #GrassmoorAVPlayer:buffer-size property.
 *
 * Returns: the value of #GrassmoorAVPlayer:buffer-size property
 */
gint
grassmoor_av_player_get_buffer_size (GrassmoorAVPlayer *self)
{
  g_return_val_if_fail (GRASSMOOR_IS_AV_PLAYER (self), 0);

  return get_player_buffer_size (self);
}


/**
 * grassmoor_av_player_set_buffer_size:
 * @self: a #GrassmoorAVPlayer
 * @buffer_size: the desired buffer size
 *
 * Change the value of the #GrassmoorAVPlayer:buffer-size property.
 * Only supported if #GrassmoorAVPlayer:can-buffer is %TRUE.
 */
void
grassmoor_av_player_set_buffer_size (GrassmoorAVPlayer *self,
                                     gint buffer_size)
{
  g_return_if_fail (GRASSMOOR_IS_AV_PLAYER (self));

  set_player_buffer_size (self, buffer_size);
}
