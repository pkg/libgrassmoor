/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

 /**
 * GrassmoorCamera::GrassmoorCamera:
 * @Title:GrassmoorCamera
 * @Short_Description: A player of camera streams.
 *
 * #GrassmoorCamera represents camera player using the #ClutterGstCamera and
 * plays camera streams.This is a wrapper library for ClutterGstCamera.
 *
 * GrassmoorCamera is available since grassmoor 1.0
 */

#include <glib.h>
#include "../libgrassmoor-camera-capture.h"

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

G_DEFINE_TYPE (GrassmoorCamera, grassmoor_camera, CLUTTER_GST_TYPE_CAMERA)

struct _GrassmoorCameraPrivate
{
	gpointer ptr;
};

#define GRASSMOOR_CAMERA_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), GRASSMOOR_TYPE_CAMERA, GrassmoorCameraPrivate))

static void grassmoor_camera_finalize (GObject *object)
{
	DEBUG ("entered");

	G_OBJECT_CLASS (grassmoor_camera_parent_class)->finalize (object);
}

static void grassmoor_camera_dispose (GObject *object)
{
	if(!GRASSMOOR_IS_CAMERA(object))
		return;

	DEBUG ("entered");

	G_OBJECT_CLASS (grassmoor_camera_parent_class)->dispose (object);
}

static void grassmoor_camera_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
	switch(propertyId)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_camera_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
	switch(propertyId)
	{
		/* IN case the property is not installed for this object, throw
		 * an appropriate error */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

static void grassmoor_camera_class_init (GrassmoorCameraClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	/* Assign pointers to our functions */
	o_class->finalize = grassmoor_camera_finalize;
	o_class->dispose = grassmoor_camera_dispose;
	o_class->set_property = grassmoor_camera_set_property;
	o_class->get_property = grassmoor_camera_get_property;

	/* Make the private structure a part of the class */
	g_type_class_add_private (klass, sizeof (GrassmoorCameraPrivate));
}

static void grassmoor_camera_init (GrassmoorCamera *camera)
{
	/* Get the private structure */ 
	//GrassmoorCameraPrivate *priv = GRASSMOOR_CAMERA_GET_PRIVATE(camera); 
}
/*
 * Public symbols
 */

/**
 * grassmoor_camera_new:
 *
 * Creates an grassmoor camera.
 *
 * Returns: (transfer full): the newly created camera
 */
GrassmoorCamera* grassmoor_camera_new()
{
	return g_object_new (GRASSMOOR_TYPE_CAMERA, NULL);
}

/**
 * grassmoor_camera_set_filter:
 * @camera: a #GrassmoorCamera
 * @filter: a #GstElement for the filter
 *
 * Set the filter element to be used.
 * Filters can be used for effects, image processing, etc.
 *
 * Returns: %TRUE on success, %FALSE otherwise
 */
gboolean grassmoor_camera_set_filter (GrassmoorCamera  *camera, GstElement *filter)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_filter (CLUTTER_GST_CAMERA(camera), filter);
}

/**
 * grassmoor_camera_remove_filter:
 * @camera: a #GrassmoorCamera
 *
 * Remove the current filter, if any.
 *
 * Returns: %TRUE on success, %FALSE otherwise
 */
gboolean grassmoor_camera_remove_filter (GrassmoorCamera *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_filter (CLUTTER_GST_CAMERA(camera), NULL);
}

/**
 * grassmoor_camera_is_ready_for_capture:
 * @camera: a #GrassmoorCamera
 *
 * Check whether the @camera is ready for video/photo capture.
 *
 * Returns: %TRUE if @camera is ready for capture, %FALSE otherwise
 */
gboolean grassmoor_camera_is_ready_for_capture (GrassmoorCamera *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_is_ready_for_capture (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_is_recording_video:
 * @camera: a #GrassmoorCamera
 *
 * Check whether the @camera is recording video.
 *
 * Returns: %TRUE if @camera is recording video, %FALSE otherwise
 */
gboolean grassmoor_camera_is_recording_video (GrassmoorCamera *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_is_recording_video (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_set_video_profile:
 * @camera: a #GrassmoorCamera
 * @profile: A #GstEncodingProfile to be used for video recording.
 *
 * Set the encoding profile to be used for video recording.
 * The default profile saves videos as Ogg/Theora videos.
 */
void grassmoor_camera_set_video_profile (GrassmoorCamera *camera, GstEncodingProfile *profile)
{
	if(! GRASSMOOR_IS_CAMERA(camera))
		return;

	return clutter_gst_camera_set_video_profile (CLUTTER_GST_CAMERA(camera), profile);
}

/**
 * grassmoor_camera_start_video_recording:
 * @camera,: a #GrassmoorCamera
 * @filename: the name of the video file to where the
 * recording will be saved
 *
 * Start a video recording with the @camera, and save it to @filename.
 * This method requires that @camera, is playing and ready for capture.
 *
 * The ::video-saved signal will be emitted when the video is saved.
 *
 * Returns: %TRUE if the video recording was successfully started, %FALSE otherwise
 */
gboolean grassmoor_camera_start_video_recording (GrassmoorCamera *camera, const gchar *filename)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_start_video_recording (CLUTTER_GST_CAMERA(camera), filename);
}

/**
 * grassmoor_camera_stop_video_recording:
 * @camera: a #GrassmoorCamera
 *
 * Stop recording video on the @camera.
 */
void grassmoor_camera_stop_video_recording (GrassmoorCamera *camera)
{
	if(! GRASSMOOR_IS_CAMERA(camera))
		return;

	return clutter_gst_camera_stop_video_recording (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_set_photo_profile:
 * @camera: a #GrassmoorCamera
 * @profile: A #GstEncodingProfile to be used for photo captures.
 *
 * Set the encoding profile to be used for photo captures.
 * The default profile saves photos as JPEG images.
 */
void grassmoor_camera_set_photo_profile (GrassmoorCamera *camera, GstEncodingProfile *profile)
{
	if(! GRASSMOOR_IS_CAMERA(camera))
		return;

	clutter_gst_camera_set_photo_profile (CLUTTER_GST_CAMERA(camera), profile);
}

/**
 * grassmoor_camera_take_photo:
 * @camera: a #GrassmoorCamera
 * @filename: the name of the file to where the
 * photo will be saved
 *
 * Take a photo with the @camera and save it to @filename.
 * This method requires that @camera is playing and ready for capture.
 *
 * The ::photo-saved signal will be emitted when the video is saved.
 *
 * Returns: %TRUE if the photo was successfully captured, %FALSE otherwise
 */
gboolean grassmoor_camera_take_photo (GrassmoorCamera *camera, const gchar *filename)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_take_photo (CLUTTER_GST_CAMERA(camera), filename);
}

/**
 * grassmoor_camera_take_photo_pixbuf:
 * @camera: a #GrassmoorCamera
 *
 * Take a photo with the @camera and emit it in the ::photo-taken signal as a
 * #GdkPixbuf.
 * This method requires that @camera is playing and ready for capture.
 *
 * Returns: %TRUE if the photo was successfully captured, %FALSE otherwise
 */
gboolean grassmoor_camera_take_photo_pixbuf (GrassmoorCamera *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_take_photo_pixbuf (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_get_camera_device:
 * @camera: a #GrassmoorCamera
 *
 * Retrieve the current selected camera device.
 *
 * Returns: (transfer none): The currently selected camera device
 */
GrassmoorCameraDevice* grassmoor_camera_get_camera_device (GrassmoorCamera  *camera)
{
	if(GRASSMOOR_IS_CAMERA(camera))
		return (GrassmoorCameraDevice*) clutter_gst_camera_get_camera_device (CLUTTER_GST_CAMERA(camera));

	return NULL;
}

/**
 * grassmoor_camera_set_camera_device:
 * @camera: a #GrassmoorCamera
 * @cameraDevice: a #GrassmoorCameraDevice
 *
 * Set the new active camera device.
 *
 * Returns: %TRUE on success, %FALSE otherwise
 */
gboolean  grassmoor_camera_set_camera_device (GrassmoorCamera  *camera, GrassmoorCameraDevice *cameraDevice)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_camera_device (CLUTTER_GST_CAMERA(camera), CLUTTER_GST_CAMERA_DEVICE(cameraDevice));
}

/**
 * grassmoor_camera_supports_gamma_correction:
 * @camera: a #GrassmoorCamera
 *
 * Check whether the @camera supports gamma correction.
 *
 * Returns: %TRUE if @camera supports gamma correction, %FALSE otherwise
 */
gboolean grassmoor_camera_supports_gamma_correction (GrassmoorCamera  *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_supports_gamma_correction (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_get_gamma_range:
 * @camera: a #GrassmoorCamera
 * @minValue: (out) (optional): Pointer to store the minimum gamma value, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum gamma value, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default gamma value, or %NULL
 *
 * Retrieve the minimum, maximum and default gamma values.
 *
 * This method will return FALSE if gamma correction is not
 * supported on @camera.
 * See grassmoor_camera_supports_gamma_correction().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_gamma_range  (GrassmoorCamera *camera, gdouble *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_gamma_range (CLUTTER_GST_CAMERA(camera), minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_gamma:
 * @camera: a #GrassmoorCamera
 * @currentValue: (out) (optional): Pointer to store the current gamma value
 *
 * Retrieve the current gamma value.
 *
 * This method will return FALSE if gamma correction is not
 * supported on @camera.
 * See grassmoor_camera_supports_gamma_correction().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_gamma (GrassmoorCamera *camera, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_gamma (CLUTTER_GST_CAMERA(camera), currentValue);
}

/**
 * grassmoor_camera_set_gamma:
 * @camera: a #GrassmoorCamera
 * @value: The value to set
 *
 * Set the gamma value.
 * Allowed values can be retrieved with
 * grassmoor_camera_get_gamma_range().
 *
 * This method will return FALSE if gamma correction is not
 * supported on @camera.
 * See grassmoor_camera_supports_gamma_correction().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */

gboolean grassmoor_camera_set_gamma (GrassmoorCamera *camera, gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_gamma (CLUTTER_GST_CAMERA(camera), value);
}

/**
 * grassmoor_camera_supports_color_balance:
 * @camera: a #GrassmoorCamera
 *
 * Check whether the @camera supports color balance.
 *
 * Returns: %TRUE if @camera supports color balance, %FALSE otherwise
 */
gboolean  grassmoor_camera_supports_color_balance (GrassmoorCamera  *camera)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_supports_color_balance (CLUTTER_GST_CAMERA(camera));
}

/**
 * grassmoor_camera_get_color_balance_property_range:
 * @camera: a #GrassmoorCamera
 * @property: Property name
 * @minValue: (out) (optional): Pointer to store the minimum value of @property, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum value of @property, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default value of @property, or %NULL
 *
 * Retrieve the minimum, maximum and default values for the color balance property @property,
 *
 * This method will return FALSE if @property does not exist or color balance is not
 * supported on @camera.
 * See grassmoor_camera_supports_color_balance().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean  grassmoor_camera_get_color_balance_property_range (GrassmoorCamera  *camera, const gchar *property, gdouble *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_color_balance_property_range (CLUTTER_GST_CAMERA(camera), property, minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_color_balance_property:
 * @camera: a #GrassmoorCamera
 * @property: Property name
 * @currentValue: (out) (optional): Pointer to store the current value of @property
 *
 * Retrieve the current value for the color balance property @property,
 *
 * This method will return FALSE if @property does not exist or color balance is not
 * supported on @camera.
 * See grassmoor_camera_supports_color_balance().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_color_balance_property (GrassmoorCamera  *camera, const gchar *property, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_color_balance_property (CLUTTER_GST_CAMERA(camera), property, currentValue);
}

/**
 * grassmoor_camera_set_color_balance_property:
 * @camera: a #GrassmoorCamera
 * @property: Property name
 * @value: The value to set
 *
 * Set the value for the color balance property @property to @value.
 * Allowed values can be retrieved with
 * clutter_gst_camera_get_color_balance_property_range().
 *
 * This method will return FALSE if @property does not exist or color balance is not
 * supported on @camera.
 * See grassmoor_camera_supports_color_balance().
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_set_color_balance_property (GrassmoorCamera  *camera, const gchar *property,gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_color_balance_property (CLUTTER_GST_CAMERA(camera), property, value);
}

/**
 * grassmoor_camera_get_brightness_range:
 * @camera: a #GrassmoorCamera
 * @minValue: (out) (optional): Pointer to store the minimum value of brightness, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum value of brightness, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default value of brightness, or %NULL
 *
 * Retrieve the minimum, maximum and default values for the brightness property,
 *
 * This method will return FALSE if brightness property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_brightness_range (GrassmoorCamera  *camera, gdouble *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_brightness_range (CLUTTER_GST_CAMERA(camera), minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_brightness:
 * @camera: a #GrassmoorCamera
 * @currentValue: (out) (optional): Pointer to store the current value of brightness, or %NULL
 *
 * Retrieve the current values for the brightness property of color balance.
 *
 * This method will return FALSE if brightness property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_brightness (GrassmoorCamera  *camera, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_brightness (CLUTTER_GST_CAMERA(camera), currentValue);
}

/**
 * grassmoor_camera_set_brightness:
 * @camera: a #GrassmoorCamera
 * @value: current value of brightness
 *
 * Set the current value for the brightness property of color balance.
 *
 * This method will return FALSE if brightness property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_set_brightness (GrassmoorCamera  *camera, gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_brightness (CLUTTER_GST_CAMERA(camera), value);
}

/**
 * grassmoor_camera_get_contrast_range:
 * @camera: a #GrassmoorCamera
 * @minValue: (out) (optional): Pointer to store the minimum value of contrast, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum value of contrast, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default value of contrast, or %NULL
 *
 * Retrieve the minimum, maximum and default values for the contrast property,
 *
 * This method will return FALSE if contrast, property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_contrast_range (GrassmoorCamera  *camera, gdouble *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_contrast_range (CLUTTER_GST_CAMERA(camera), minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_contrast:
 * @camera: a #GrassmoorCamera
 * @currentValue: (out) (optional): Pointer to store the current value of contrast, or %NULL
 *
 * Retrieve the current values for the contrast of color balance property,
 *
 * This method will return FALSE if contrast property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_contrast (GrassmoorCamera  *camera, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_contrast (CLUTTER_GST_CAMERA(camera), currentValue);
}

/**
 * grassmoor_camera_set_contrast:
 * @camera: a #GrassmoorCamera
 * @value: current value of contrast
 *
 * Set the current value for the contrast property of color balance.
 *
 * This method will return FALSE if contrast property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_set_contrast (GrassmoorCamera  *camera, gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_contrast (CLUTTER_GST_CAMERA(camera), value);
}

/**
 * grassmoor_camera_get_saturation_range:
 * @camera: a #GrassmoorCamera
 * @minValue: (out) (optional): Pointer to store the minimum value of saturation, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum value of saturation, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default value of saturation, or %NULL
 *
 * Retrieve the minimum, maximum and default values for the saturation property,
 *
 * This method will return FALSE if saturation, property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_saturation_range  (GrassmoorCamera  *camera, gdouble *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_saturation_range (CLUTTER_GST_CAMERA(camera), minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_saturation:
 * @camera: a #GrassmoorCamera
 * @currentValue: (out) (optional): Pointer to store the current value of saturation, or %NULL
 *
 * Retrieve the current values for the saturation property of color balance,
 *
 * This method will return FALSE if saturation property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_saturation (GrassmoorCamera  *camera, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_saturation (CLUTTER_GST_CAMERA(camera), currentValue);
}

/**
 * grassmoor_camera_set_saturation:
 * @camera: a #GrassmoorCamera
 * @value: current value of saturation
 *
 * Set the current value for the saturation property of color balance.
 *
 * This method will return FALSE if saturation property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean  grassmoor_camera_set_saturation (GrassmoorCamera  *camera, gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_saturation (CLUTTER_GST_CAMERA(camera), value);
}

/**
 * grassmoor_camera_get_hue_range:
 * @camera: a #GrassmoorCamera
 * @minValue: (out) (optional): Pointer to store the minimum value of hue, or %NULL
 * @maxValue: (out) (optional): Pointer to store the maximum value of hue, or %NULL
 * @defaultValue: (out) (optional): Pointer to store the default value of hue, or %NULL
 *
 * Retrieve the minimum, maximum and default values for the hue property,
 *
 * This method will return FALSE if hue, property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_get_hue_range (GrassmoorCamera  *camera, gdouble  *minValue, gdouble *maxValue, gdouble *defaultValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_hue_range (CLUTTER_GST_CAMERA(camera), minValue, maxValue, defaultValue);
}

/**
 * grassmoor_camera_get_hue:
 * @camera: a #GrassmoorCamera
 * @currentValue: (out) (optional): Pointer to store the current value of hue, or %NULL
 *
 * Retrieve the current values for the hue property of color balance,
 *
 * This method will return FALSE if hue property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean  grassmoor_camera_get_hue  (GrassmoorCamera  *camera, gdouble *currentValue)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_get_hue (CLUTTER_GST_CAMERA(camera), currentValue);
}

/**
 * grassmoor_camera_set_hue:
 * @camera: a #GrassmoorCamera
 * @value: current value of hue
 *
 * Set the current value for the hue property of color balance.
 *
 * This method will return FALSE if hue property does not exist or color balance is not
 * supported on @camera.
 *
 * Returns: %TRUE if successful, %FALSE otherwise
 */
gboolean grassmoor_camera_set_hue (GrassmoorCamera  *camera, gdouble value)
{
	g_return_val_if_fail (GRASSMOOR_IS_CAMERA (camera), FALSE);

	return clutter_gst_camera_set_hue (CLUTTER_GST_CAMERA(camera), value);
}

/**
 * grassmoor_camera_get_filter:
 * @camera: a #GrassmoorCamera
 *
 * Retrieve the current filter being used.
 *
 * Returns: (transfer none) (nullable): The current filter or %NULL if none is set
 */
GstElement * grassmoor_camera_get_filter (GrassmoorCamera  *camera)
{
	if(GRASSMOOR_IS_CAMERA(camera))
		return clutter_gst_camera_get_filter (CLUTTER_GST_CAMERA(camera));

	return NULL;
}
