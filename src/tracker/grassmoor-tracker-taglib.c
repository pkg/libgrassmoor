/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <libgrassmoor-tracker.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <tag_c.h>
#include <gst/gst.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grassmoor-tracker-internal.h"

static gchar *get_mime_type (const gchar *pUri);
static guint64 get_file_size(const gchar *pUri);
static void get_aduio_meta_info(const gchar *pFileName, GrassmoorMediaInfo *pResultStructure);
static GdkPixbuf *get_pixbuf_for_thumbnail(const char *picData, size_t size);
static gchar *create_thumbnails_dir(gchar *pMime);
static const gchar* get_thumbpath_for_title(const gchar *md5Title, const gchar *pUri);
static gchar *save_thumbnail (GdkPixbuf *pBuffer, const gchar *pFilename);
static void  tracker_get_video_details(const gchar *fileName, gchar **thumbnailPath);
static GstElement* create_pipeline(const gchar *fileName);
static gboolean align_state_to_retrieve_frame(GstElement *pipeline);
static void seek_to_position(GstElement *pipeline);
static gchar* get_video_thumbnail(GstSample *sample, gint width, gint height, const gchar *fileName);

#define TRACKER_MIME_TYPE_AUDIO	"audio"
#define TRACKER_MIME_TYPE_VIDEO	"video"
#define TRACKER_MIME_TYPE_IMAGE	"image"

#define CAPS "video/x-raw,format=RGB,width=160,pixel-aspect-ratio=1/1"

//static gchar *ThumbsDir = NULL;

/************************* EXTRACTION OF DATA FROM TAGLIB
 * ******************/

/**
 * get_mime_type:
 * @pUri: File path for whom the mime type needs to be returned
 * Returns: The mime type of the file
 *
 * Get the mime type and the media type for a given uri
 */
static gchar *get_mime_type (const gchar *pUri)
{
	gchar *content_type, *pBaseMime = NULL;
 	gchar **parts;

	if (pUri == NULL)
		return NULL;
	DEBUG ("entered");

	// Get the URI’s Content Type.
	content_type = g_content_type_guess (pUri, NULL, 0, NULL);
	parts = g_strsplit (content_type, "/", 2);
	g_free (content_type);	

	pBaseMime = g_strdup (parts[0]);
	g_strfreev (parts);

	DEBUG ("pBaseMime %s", pBaseMime);

	return pBaseMime;
	/*if(pBaseMime)
		return g_strdup (pBaseMime);
	else
		return NULL;*/
}

static guint64 get_file_size(const gchar *pUri)
{
	GFile *file;
	GFileInfo *info;
	guint64 size;

	if (pUri == NULL)
		return 0;
	DEBUG ("entered");

	// Get the URI’s info.
	file = g_file_new_for_uri (pUri);
	info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_FILE_QUERY_INFO_NONE, NULL, NULL);
	g_object_unref (file);

	if (info == NULL)
		return 0;

	size = g_file_info_get_size (info);
	g_object_unref (info);
	return size;
}


/*
Function: get_aduio_meta_info
Description: get all meta info for the audio file
Parameters: Name of the file and instance of GrassmoorMediaInfo* where it
is to be filled up
Return Value: void
 **/
static void get_aduio_meta_info(const gchar *pFileName, GrassmoorMediaInfo *pResultStructure)
{
	TagLib_File *pTagLibFile = NULL;
	TagLib_Tag *pTag = NULL;
	const TagLib_AudioProperties *pTaglibProperties = NULL;
	//gint inPicSize = 0;

	pTagLibFile = taglib_file_new(pFileName);

	if(NULL != pTagLibFile)
	{
		DEBUG ("Trying to get meta data ...");
		/*Use taglib and populate all meta info,
		  artist, genre, title etc */
		pTag = taglib_file_tag(pTagLibFile);
		pTaglibProperties = taglib_file_audioproperties(pTagLibFile);

		if( (NULL != pTag) && (NULL != pTaglibProperties) )
		{
			pResultStructure->Info.AudioVideoFileInfo.artist = g_strdup (taglib_tag_artist (pTag));
			DEBUG ("data->artist = %s", pResultStructure->Info.AudioVideoFileInfo.artist);

			pResultStructure->Info.AudioVideoFileInfo.genre = g_strdup (taglib_tag_genre (pTag));
			DEBUG ("data->genre = %s", pResultStructure->Info.AudioVideoFileInfo.genre);

			pResultStructure->Info.AudioVideoFileInfo.iTrackNum = taglib_tag_track (pTag);
			DEBUG ("data->trackNum =%d", pResultStructure->Info.AudioVideoFileInfo.iTrackNum);

			pResultStructure->Info.AudioVideoFileInfo.album = g_strdup (taglib_tag_album (pTag));
			DEBUG ("data->album = %s", pResultStructure->Info.AudioVideoFileInfo.album);

			pResultStructure->title = g_strdup (taglib_tag_title (pTag));
			DEBUG ("data->title = %s", pResultStructure->title);

			pResultStructure->Info.AudioVideoFileInfo.year = g_strdup_printf ("%d", taglib_tag_year(pTag));
			DEBUG ("year    - %s", pResultStructure->Info.AudioVideoFileInfo.year);

			pResultStructure->Info.AudioVideoFileInfo.iDuration = taglib_audioproperties_length(pTaglibProperties);
			DEBUG ("length = %d", pResultStructure->Info.AudioVideoFileInfo.iDuration);

                        /* FIXME: are we supposed to call taglib_tag_free_strings()
                         * at some point to free the allocated strings in taglib? */
		}

		taglib_file_free(pTagLibFile);
	}

# if 0
	inPicSize = taglib_file_get_apic_size(pTagLibFile);
	char *picData = malloc(inPicSize);
	taglib_file_attached_picture(pTagLibFile, picData);
	if(NULL != picData)
		free(picData);
# endif
}

/**
 * Function:    get_pixbuf_for_thumbnail
 * Description: Get the thumbnail pixbuf
 * Parameters:  thumbnail data and size
 * Return:      The pixbuf image for the thumbnail
 **/
static GdkPixbuf *get_pixbuf_for_thumbnail(const char *picData, size_t size)
{
	GError *pError = NULL;
	GdkPixbuf *pixbuf = NULL;
	/* Create a GdkPixbuf from the data */
	if (picData)
	{
		GdkPixbufLoader *pLoader;
		pLoader = gdk_pixbuf_loader_new();
		if (!gdk_pixbuf_loader_write(pLoader,
					(const guchar *) picData,
					size,
					&pError))
		{
			g_warning("Error decoding image: %s", pError->message);
			g_error_free(pError);
		}
		else
		{
			if (!gdk_pixbuf_loader_close(pLoader, &pError))
				g_error_free(pError);
			else
			{
				pixbuf = g_object_ref(
						gdk_pixbuf_loader_get_pixbuf(pLoader));

			}
		}
		if(G_IS_OBJECT(pLoader))
			g_object_unref(pLoader);

		return pixbuf;
	}
	return NULL;
}

/**
 * create_thumbnails_dir:
 *
 * Create the default system thumbnail directory. Thumbnails of all
 * media types (image, audio, video are stored here and the uris of
 * these are made available to the applilcation)
 */
static gchar* create_thumbnails_dir(gchar *pBaseMime)
{
	//if(NULL == ThumbsDir || ! g_file_test (ThumbsDir, G_FILE_TEST_IS_DIR))
	gchar *ThumbsDir = NULL;
	/*Create the directory */
	if(g_strcmp0(pBaseMime, TRACKER_MIME_TYPE_AUDIO) == 0)
		ThumbsDir = (gchar *)g_build_filename (g_get_home_dir(), ".cache","media-art" , NULL);
	else
		ThumbsDir = (gchar *)g_build_filename (g_get_home_dir(), ".cache", "thumbnails", "normal", NULL);
		
	if(ThumbsDir && ! g_file_test (ThumbsDir, G_FILE_TEST_IS_DIR))
		g_mkdir_with_parents (ThumbsDir, 0755);

	return ThumbsDir;
}

/**
 * Function : get_thumbpath_for_title
 * Description: Get the thumbnail path
 * Parameters: the md5title that was generated
 * Return value: Thumbnail Path
 **/
static const gchar* get_thumbpath_for_title(const gchar *md5Title, const gchar *pUri)
{
	if(NULL == md5Title)
		return NULL;

	/*If the thumbnails dir has not been created, do the same */
//	if(NULL == ThumbsDir)
	gchar *ThumbsDir = create_thumbnails_dir(get_mime_type(pUri));

	/*Return the full uri of the thumbnail*/
	const gchar*pThumbPath = g_strconcat (ThumbsDir, "/", md5Title, ".png", NULL);
	g_free(ThumbsDir);
	g_free((gchar *)md5Title);
	
	return pThumbPath;
}

/**
 * Function:    save_thumbnail
 * Description: Save thumbnail data into given file
 * Parameters:  Thumbnail pixbuf data buffer and filename
 * Return:      Thumbnail path
 **/
static gchar *save_thumbnail (GdkPixbuf *pBuffer, const gchar *pFilename)
{
	GError *pError = NULL;
	gchar *pThumbnail = NULL;
	GdkPixbuf *pScaledPixbuf = NULL;
	gboolean bSavePixbuf = FALSE;
	const gchar *pThumbUri = NULL;

	if (pFilename == NULL)
		return NULL;

	if (pBuffer == NULL)
		pScaledPixbuf = gdk_pixbuf_new_from_file_at_size (pFilename, 162, 162, NULL);
	else
		pScaledPixbuf = gdk_pixbuf_scale_simple (pBuffer, 162, 162, GDK_INTERP_BILINEAR);

	if (pScaledPixbuf == NULL)
		return NULL;

	//g_print("file uri = %s\n", g_filename_to_uri (pFilename, NULL, NULL));
	pThumbUri = tracker_get_thumbnail_path_for_string(g_filename_to_uri (pFilename, NULL, NULL));

	bSavePixbuf = gdk_pixbuf_save (pScaledPixbuf, pThumbUri, "png", &pError, NULL);
	if (bSavePixbuf && NULL == pError)
		pThumbnail = g_strdup (pThumbUri);

	g_free ((gchar *)pThumbUri);
	if(G_IS_OBJECT(pScaledPixbuf))
		g_object_unref (pScaledPixbuf);

	return pThumbnail;
}

static gchar* get_video_thumbnail(GstSample *pSample, gint inWidth, gint inHeight, const gchar *pFileName)
{
	GdkPixbuf *pixbuf = NULL;
	GstMapInfo mapInfo;
	gchar* pThumbnail = NULL;

	if (pSample) 
	{
		GstBuffer *pBuffer;
		GstCaps *pCaps;
		GstStructure *pStruct;
		gboolean bRes;
		/* To get rgb buffer, set the caps on the appsink. 
		 * Height need not to be mentioned because it is depend on  aspect-ratio of pixel
                 * of the source material */
		pCaps = gst_sample_get_caps (pSample);
		if (pCaps == NULL) {
			WARNING ("snapshot format not valid");
			exit (-1);
		}
		pStruct = gst_caps_get_structure (pCaps, 0);

		/* now on the buffer, get the final caps to get size */
		bRes = gst_structure_get_int (pStruct, "width", &inWidth);
		bRes |= gst_structure_get_int (pStruct, "height", &inHeight);
		if (bRes == FALSE) {
			WARNING ("snapshot dimension not valid");
			exit (-1);
		}

		/* now we need to create pixmap from buffer. After creation of picmap, save it,
		 * video buffers of gstreamer stride is rounded up to the nearest multiple of 4 */
		pBuffer = gst_sample_get_buffer (pSample);
		gst_buffer_map(pBuffer, &mapInfo, GST_MAP_READ);
		pixbuf = gdk_pixbuf_new_from_data ( mapInfo.data, GDK_COLORSPACE_RGB, FALSE, 8, inWidth, inHeight,
							GST_ROUND_UP_4 (inWidth * 3), NULL, NULL);
		if(pixbuf)
		{
				pThumbnail = save_thumbnail (pixbuf, g_filename_from_uri(pFileName, NULL, NULL));
		}
		gst_buffer_unmap (pBuffer, &mapInfo);
		return pThumbnail;
	}
	return pThumbnail;
}


/**
Function: tracker_get_video_details
Description: Populate all meta info for the video file
Parameters: Name of the file
Return Value: thumbnailpath
 **/
static void  tracker_get_video_details(const gchar *fileName, gchar **thumbnail)
{
	if(NULL != fileName)
	{
		GstElement *sink;

		gint width, height;
		GstSample *sample;

		width = height = 0;
		/* create a new pipeline */
		GstElement *pipeline = create_pipeline(fileName);
		if(NULL != pipeline)
		{
			/* get sink */
			sink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");

			gboolean retVal = align_state_to_retrieve_frame(pipeline);
			if(FALSE != retVal)
			{
				seek_to_position(pipeline);

				/* get the preroll buffer from appsink, this block untils appsink really
				 * prerolls */
				g_signal_emit_by_name (sink, "pull-preroll", &sample, NULL);
				gst_object_unref (sink);
				if (NULL != sample)
				{
					*thumbnail = get_video_thumbnail(sample, width, height, fileName);

					/* cleanup */
					gst_element_set_state (pipeline, GST_STATE_NULL);
					gst_object_unref (pipeline);

				}
			}
		}
	}
}

static GstElement* create_pipeline(const gchar *fileName)
{
	GstElement *pipeline = NULL;
	GError *error = NULL;

	char *description = g_strdup_printf ("uridecodebin uri=\"%s\" ! videoconvert ! videoscale ! " " appsink name=sink caps=\"" CAPS "\"", fileName);
	pipeline = gst_parse_launch (description, &error);

	if (error != NULL)
	{
		WARNING ("%s: code %d: %s", g_quark_to_string (error->domain), error->code, error->message);
		g_error_free (error);
		return NULL;
	}

	return pipeline;
}

static gboolean align_state_to_retrieve_frame(GstElement *pipeline)
{
	GstStateChangeReturn stateChange;

	/* set to PAUSED to make the first frame arrive in the sink */
	stateChange = gst_element_set_state (pipeline, GST_STATE_PAUSED);
	if( (stateChange == GST_STATE_CHANGE_FAILURE) || (stateChange == GST_STATE_CHANGE_NO_PREROLL))
		return FALSE;

	if(GST_STATE_CHANGE_FAILURE == gst_element_get_state (pipeline, NULL, NULL, 5 * GST_SECOND))
		return FALSE;

	return TRUE;
}

static void seek_to_position(GstElement *pipeline)
{
	/* get the duration */
	GstFormat timeFormat;
	gint64 duration, position;

	timeFormat = GST_FORMAT_TIME;
	gst_element_query_duration (pipeline, timeFormat, &duration);

	if (duration != -1)
		/* we have a duration, seek to 5% */
		position = duration * 5 / 100;
	else
		/* no duration, seek to 1 second, this could EOS */
		position = 1 * GST_SECOND;

	gst_element_seek_simple (pipeline, GST_FORMAT_TIME, GST_SEEK_FLAG_KEY_UNIT | GST_SEEK_FLAG_FLUSH, position);
}

/************************************************************************
 * tracker_taglib_get_thumbnail:
 * @uri: File path for which the thumbnail needs to be created.
 * Returns: The file path for the thumbnail file
 *
 * Create the thumbnail for a given file uri (It could be of any media
 * type, i.e. audio, video, image). The created thumbnail is stored in
 * the thumbnail directory and the path of the thumbnail is returned to
 * the calling application
 *************************************************************************/
gchar* tracker_taglib_get_thumbnail(const gchar *pUri)
{
	gchar * pThumbnail = NULL;
	if(NULL != pUri)
	{
		/*BUild the filename from the recieved URI */
		gchar *pFilename = g_filename_from_uri(pUri, NULL, NULL);
		if(NULL != pFilename)
		{
			gchar *pCoverArt = NULL;
			pCoverArt = (gchar*)tracker_get_thumbnail_path_for_string(g_filename_to_uri (pFilename, NULL, NULL));
			if(pCoverArt)
			{
				if(tracker_check_thumb_exists(pCoverArt) == TRUE)
				{
					DEBUG ("Cover art already exists");
					return pCoverArt;
				}
			}

			gchar *pBaseMime = (gchar *) get_mime_type(pUri);
			if (NULL != pBaseMime)
			{
				if(g_strcmp0(TRACKER_MIME_TYPE_AUDIO, pBaseMime) == 0)
				{
					TagLib_File *pTaglibFile = NULL;
					pTaglibFile = taglib_file_new (pFilename);
					if (NULL != pTaglibFile)
					{
						gint inPicSize = taglib_file_get_apic_size (pTaglibFile);
						gchar *picData = (gchar *)malloc (inPicSize * sizeof(gchar));
						taglib_file_attached_picture (pTaglibFile, picData);

						GdkPixbuf *pShot = get_pixbuf_for_thumbnail (picData, inPicSize);
						pThumbnail = save_thumbnail (pShot, pFilename);

						DEBUG ("pic_size = %d thumbnail = %s", inPicSize, pThumbnail);
						if(picData)
							g_free(picData);

						if(G_IS_OBJECT(pShot))
							g_object_unref (pShot);

						taglib_file_free(pTaglibFile);
					}
				}
				else if(g_strcmp0(TRACKER_MIME_TYPE_IMAGE, pBaseMime) == 0)
					pThumbnail = save_thumbnail (NULL, pFilename);
				else if(g_strcmp0(TRACKER_MIME_TYPE_VIDEO, pBaseMime) == 0)
				{
					gchar *newname = g_filename_to_uri (pFilename, NULL, NULL);
					FILE* fp;
					gchar* thumb_file_uri = (gchar*)tracker_get_thumbnail_path_for_string(newname);
					//g_print("get thumbnail path = %s\n", thumb_file_uri);
					if ( (fp = fopen(thumb_file_uri, "r")) == NULL )
					{
						tracker_get_video_details (newname, &pThumbnail);
					}
					else
					{
						fclose (fp);
						fp = NULL;
						pThumbnail = (gchar*)tracker_get_thumbnail_path_for_string(newname);
					}
				} 
				g_free(pBaseMime);
			} 
			g_free(pFilename);

			DEBUG("Thumb path = %s", pThumbnail);
		}
	}
	return pThumbnail;
}


/**
 * tracker_taglib_get_file_info
 * @uri: File path for whom the mime info needs to be obtained
 * Returns: The mime information for the file. The details are
 *           filled  into the structure of type MediaUtilsFileInfo.
 *           All mime and meta information of the file is extracted out
 *           and returned in the structure
 *
 * Get the information for any given uri
 */
GrassmoorMediaInfo *tracker_taglib_get_file_info(const gchar *pUri)
{
	gchar *pFilename = NULL;
	gchar *pBaseMime = NULL;
	GrassmoorMediaInfo *pResultStructure = NULL;

	if(pUri == NULL)
		return NULL;
	DEBUG ("entered");

	/*BUild the filename from the recieved URI */
	pFilename = g_filename_from_uri(pUri, NULL, NULL);
	DEBUG ("Filename = %s", pFilename);

	if(pFilename == NULL)
		pFilename = g_strdup (pUri);

	pBaseMime = get_mime_type ((gchar*)pFilename);
	DEBUG ("baseMime %s", pBaseMime);

	if (pBaseMime == NULL)
		return NULL;

	pResultStructure = g_new0(GrassmoorMediaInfo, 1);
	pResultStructure->title = NULL;
	pResultStructure->enFileType = GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN;
	pResultStructure->url = g_strdup (pUri);
	pResultStructure->enSource = GRASSMOOR_TRACKER_VOLUME_LOCAL;

	pResultStructure->content_type = pBaseMime; /* transfer ownership */

	pResultStructure->iSizeInBytes = get_file_size(pFilename);  

	if (pResultStructure->url)
	{
		gchar* imageFilePath = NULL;
  		gchar* imageFileName = NULL;
  		imageFilePath = g_filename_from_uri (pResultStructure->url, NULL, NULL);
  		if (imageFilePath)
    	{
      		imageFileName = g_path_get_basename (imageFilePath);
      		g_free (imageFilePath);
    	}
  		if (imageFileName)
    	{
     	 	pResultStructure->title = g_strdup (imageFileName);
     	 	g_free (imageFileName);
    	}
	}

	if(g_strcmp0(TRACKER_MIME_TYPE_AUDIO, pBaseMime) == 0)
	{
		pResultStructure->enFileType = GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO;
		get_aduio_meta_info(pFilename, pResultStructure);

		/*Get the date */
		//get_file_date(data);
	}
	else if (g_strcmp0 (TRACKER_MIME_TYPE_IMAGE, pBaseMime) == 0)
	{
  		pResultStructure->enFileType = GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO;
	}
	else if (g_strcmp0 (TRACKER_MIME_TYPE_VIDEO, pBaseMime) == 0)
	{
  		pResultStructure->enFileType = GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO;
	}
	return pResultStructure;
}

/**
 * Function:    tracker_check_thumb_exists
 * Description: check whether albumart file exists
 * Parameters:  gchar *
 * Return:      gboolean
 **/
gboolean tracker_check_thumb_exists (const gchar *pThumbPath)
{
	if(NULL != pThumbPath)
	{
		/* check whether file exists */
		GFile *pFile = g_file_new_for_commandline_arg (pThumbPath);
		if (pFile != NULL)
		{
			if(g_file_query_exists(pFile, NULL))
				return TRUE;
		}
	}
	return FALSE;
}

/**
 * tracker_get_thumbnail_path_for_string:
 * @string: Input file string
 * Returns: Retursn the thumbnail file path
 *
 * Get the thumbnail path for the given file name (The thumbnail path is
 * encoded using teh MD5 Checksum algorithm. This new path of file is
 * returned)
 */
const gchar* tracker_get_thumbnail_path_for_string(const gchar *string)
{
	if(NULL == string)
		return NULL;

	/*Compute the md5checksum name */
	gchar *md5Title = g_compute_checksum_for_string (G_CHECKSUM_MD5, string, -1);

	if(NULL != md5Title)
		DEBUG ("Taglib generated md5 for thumb = %s:", md5Title);
	/*Pass the md5checksum name and get the entire URI */
	return get_thumbpath_for_title(md5Title, string);
}
