/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LIBGRASSMOOR_TRACKER_H__
#define __LIBGRASSMOOR_TRACKER_H__
#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/**
 * SECTION:tracker/libgrassmoor-tracker.h
 * @Title: Libgrassmoor Tracker
 * @Short_Description: Abstraction layer for tracker search engine
 * 
 * Tracker is an abstraction layer for tracker search engine which in turn uses W3C standards for RDF 
 * ontologies using Nepomuk with SPARQL to query and update the meta data. Metatracker apis provides access 
 * to media content and its meta-data in the local file system  as well as for removable media. Metatracker APIs are
 * very simple to use and makes media search easy by avoiding writing complex sparql queries.
 * 
 * GrassmoorTracker is available since libgrassmoor 1.0
 */

#define GRASSMOOR_TYPE_TRACKER (grassmoor_tracker_get_type ())
G_DECLARE_FINAL_TYPE (GrassmoorTracker, grassmoor_tracker, GRASSMOOR, TRACKER, GObject)

//#define GRASSMOOR_TRACKER_MEDIA_INFO_LENGTH (255)
//#define GRASSMOOR_TRACKER_MEDIA_MAX_PATH (PATH_MAX)
typedef struct _GrassmoorMediaInfo GrassmoorMediaInfo;

/**
 * GrassmoorQueryOrderType:
 * @GRASSMOOR_TRACKER_ORDER_TYPE_ASC: sorting of URL List in Ascending order
 * @GRASSMOOR_TRACKER_ORDER_TYPE_DESC: sorting of URL List in Descending order
 */
typedef enum
{
	GRASSMOOR_TRACKER_ORDER_TYPE_ASC,
	GRASSMOOR_TRACKER_ORDER_TYPE_DESC
} GrassmoorQueryOrderType;

/**
 * GrassmoorMediaType:
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO: Media File Type for Audio Files
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO: Media File Type for Video Files
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO: Media File Type for Photo Files
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST: Media File Type for Play-list Files
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_DOC: File Media Type for document Files
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER: Media Type for Folders
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_FILE: File Type for Any File
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN: Media Type not known
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_ALL: All of above types
 * @GRASSMOOR_TRACKER_MEDIA_TYPE_COUNT: Total count of MEDIA_TYPE
 * This enum value is Required for asking information or list of particular media type
 */
typedef enum
{
	GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,
	GRASSMOOR_TRACKER_MEDIA_TYPE_VIDEO,
	GRASSMOOR_TRACKER_MEDIA_TYPE_PHOTO ,
	GRASSMOOR_TRACKER_MEDIA_TYPE_PLAYLIST,
	GRASSMOOR_TRACKER_MEDIA_TYPE_DOC,
	GRASSMOOR_TRACKER_MEDIA_TYPE_FOLDER,
	GRASSMOOR_TRACKER_MEDIA_TYPE_FILE,
	GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN,
	GRASSMOOR_TRACKER_MEDIA_TYPE_ALL,
	GRASSMOOR_TRACKER_MEDIA_TYPE_COUNT	/* This must be last element in enum */
} GrassmoorMediaType;

/**
 * GrassmoorMediaInfoType:
 * @GRASSMOOR_TRACKER_MEDIA_INFO_URL: For Audio, Video, Picture, Play-list files & Folders
 * @GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE: For Audio, Video, Picture, Play-list files & Folders
 * @GRASSMOOR_TRACKER_MEDIA_INFO_MIME: For Audio, Video, Picture, Play-list files & Folders
 * @GRASSMOOR_TRACKER_MEDIA_INFO_SIZE: For Audio, Video, Picture, Play-list files & Folders
 * @GRASSMOOR_TRACKER_MEDIA_INFO_TITLE: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_GENRE: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_YEAR: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_DURATION: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM: For Audio, Video files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT: For Picture files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH: For Picture files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_TAG: For Picture files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_DATE: For Picture files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_COMMENT: For Picture files
 * @GRASSMOOR_TRACKER_MEDIA_INFO_NUM_OF_ENTRIES: For Play-list & Folders
 * @GRASSMOOR_TRACKER_MEDIA_INFO_ANY:
 * @GRASSMOOR_TRACKER_MEDIA_INFO_COUNT:
 * Type of Required information of media files.
 */
typedef enum
{
	GRASSMOOR_TRACKER_MEDIA_INFO_URL,
	GRASSMOOR_TRACKER_MEDIA_INFO_SOURCE,
	GRASSMOOR_TRACKER_MEDIA_INFO_MIME,
	GRASSMOOR_TRACKER_MEDIA_INFO_SIZE,
	GRASSMOOR_TRACKER_MEDIA_INFO_TITLE,
	GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST,
	GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM,
	GRASSMOOR_TRACKER_MEDIA_INFO_GENRE,
	GRASSMOOR_TRACKER_MEDIA_INFO_YEAR,
	GRASSMOOR_TRACKER_MEDIA_INFO_DURATION,
	GRASSMOOR_TRACKER_MEDIA_INFO_TRACK_NUM,
	GRASSMOOR_TRACKER_MEDIA_INFO_HEIGHT,
	GRASSMOOR_TRACKER_MEDIA_INFO_WIDTH,
	GRASSMOOR_TRACKER_MEDIA_INFO_TAG,
	GRASSMOOR_TRACKER_MEDIA_INFO_DATE,
	GRASSMOOR_TRACKER_MEDIA_INFO_COMMENT,
	GRASSMOOR_TRACKER_MEDIA_INFO_NUM_OF_ENTRIES,
	GRASSMOOR_TRACKER_MEDIA_INFO_ANY,
	GRASSMOOR_TRACKER_MEDIA_INFO_COUNT	/* This must be last element in enum */
} GrassmoorMediaInfoType;

/**
 * GrassmoorVolumeType:
 * @GRASSMOOR_TRACKER_VOLUME_LOCAL: Local Media Volume
 * @GRASSMOOR_TRACKER_VOLUME_REMOVABLE: Removable Media Volume - USB , SD
 * @GRASSMOOR_TRACKER_VOLUME_OPTICAL: Optical Media Volume - CD
 * @GRASSMOOR_TRACKER_VOLUME_REMOTE: Remote Media Volume - Remote link http url
 * @GRASSMOOR_TRACKER_VOLUME_ALL:
 */
typedef enum
{
	GRASSMOOR_TRACKER_VOLUME_LOCAL = 1,
	GRASSMOOR_TRACKER_VOLUME_REMOVABLE,
	GRASSMOOR_TRACKER_VOLUME_OPTICAL,
	GRASSMOOR_TRACKER_VOLUME_REMOTE,
	GRASSMOOR_TRACKER_VOLUME_ALL,
} GrassmoorVolumeType;

/**
 * GrassmoorElementType:
 * @GRASSMOOR_TRACKER_FILE: file
 * @GRASSMOOR_TRACKER_FOLDER: folder
 * @GRASSMOOR_TRACKER_BOTH_FILES_AND_FOLDER: Both Files & Folders
 */
typedef enum
{
	GRASSMOOR_TRACKER_FILE = 1,
	GRASSMOOR_TRACKER_FOLDER,
	GRASSMOOR_TRACKER_BOTH_FILES_AND_FOLDER,
} GrassmoorElementType;

/**
 * GrassmoorIndexingStatus:
 * @GRASSMOOR_TRACKER_INDEXING_IDLE: Idle
 * @GRASSMOOR_TRACKER_INDEXING_IN_PROGRESS: In Progress
 * @GRASSMOOR_TRACKER_INDEXING_COMPLETE: Complete
 * @GRASSMOOR_TRACKER_INDEXING_FAILED: Failed
 * Indicates Indexing status
 **/
typedef enum
{
	GRASSMOOR_TRACKER_INDEXING_IDLE = 1,
	GRASSMOOR_TRACKER_INDEXING_IN_PROGRESS,
	GRASSMOOR_TRACKER_INDEXING_COMPLETE,
	GRASSMOOR_TRACKER_INDEXING_FAILED,
} GrassmoorIndexingStatus;

GrassmoorTracker * grassmoor_tracker_new (void);

/* Gives list of Audio, Video, Photo, play-list Files for a particular volume*/
GPtrArray * grassmoor_tracker_get_url_list (GrassmoorTracker *self,
                                            GrassmoorVolumeType volume,
                                            const gchar *volume_mount_point,
                                            GrassmoorMediaType fileType,
                                            GrassmoorMediaInfoType enSortingType,
                                            GrassmoorQueryOrderType queryOrder,
                                            gint offset,
                                            gint iLimit,
                                            GError **error);

/* Gives detailed info for a particular Audio, Video, Photo, play-list File */
GrassmoorMediaInfo * grassmoor_tracker_get_media_file_info (GrassmoorTracker *self,
                                                            const gchar* FileUrl,
                                                            GrassmoorMediaType enFileType,
                                                            GError **error);

/* Updates given info for a particular Audio, Video, Photo, play-list File */
/*< private >*/
//gboolean grassmoor_tracker_update_media_file_info(GrassmoorMediaInfo* stFileInfo, GrassmoorMediaInfoType InfoToUpdate, GError* error);

/* Gives list of detailed GrassmoorMediaInfo for Audio, Video, Photo, play-list Files for a particular volume*/
GPtrArray * grassmoor_tracker_get_media_info_list (GrassmoorTracker *self,
                                                   GrassmoorVolumeType volume,
                                                   const gchar *volume_mount_point,
                                                   GrassmoorMediaType enFileType,
                                                   GrassmoorMediaInfoType enSortingType,
                                                   GrassmoorQueryOrderType queryOrder,
                                                   gint ioffset,
                                                   gint iLimit,
                                                   GError **error);

/* Gives List of Artists, Albums, Genres, Songs for a particular volume */
GPtrArray * grassmoor_tracker_get_meta_list (GrassmoorTracker *self,
                                             GrassmoorVolumeType volume,
                                             const gchar *volume_mount_point,
                                             GrassmoorMediaInfoType enInfoType,
                                             gint ioffset,
                                             gint iLimit,
                                             GError **error);

/* Gives required info for particular Artist, Album, etc  */
GPtrArray * grassmoor_tracker_get_meta_list_for (GrassmoorTracker *self,
                                                 GrassmoorVolumeType volume,
                                                 const gchar* volume_mount_point,
                                                 GrassmoorMediaInfoType enInputInfoType,
                                                 const gchar* sInput,
                                                 GrassmoorMediaInfoType enOutputInfoType,
                                                 gint ioffset,
                                                 gint iLimit,
                                                 GError **error);

/* Gives list of urls for a particular Playlist  */
GPtrArray * grassmoor_tracker_get_url_list_for_playlist (GrassmoorTracker *self,
                                                         const gchar* PlaylistPath,
                                                         gint ioffset,
                                                         gint iLimit,
                                                         GError **error);

/* Utils */
gboolean grassmoor_tracker_pause_indexing (GrassmoorTracker *self);
gboolean grassmoor_tracker_start_indexing (GrassmoorTracker *self);
gboolean grassmoor_tracker_force_reindexing(GError* error);
gchar * grassmoor_tracker_get_albumart_url (const gchar *pUri, const gchar *sAlbum, const gchar *sArtist);


gchar * grassmoor_tracker_generate_thumbnail_uri (const gchar* sFileUrl);

/* GrassmoorMediaInfo API */

GType grassmoor_media_info_get_type (void);

GrassmoorVolumeType grassmoor_media_info_get_source (GrassmoorMediaInfo *info);
GrassmoorMediaType grassmoor_media_info_get_media_type (GrassmoorMediaInfo *info);
gboolean grassmoor_media_info_is_audio_video (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_url (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_title (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_content_type (GrassmoorMediaInfo *info);
guint grassmoor_media_info_get_size (GrassmoorMediaInfo *info);

guint grassmoor_media_info_get_track_number (GrassmoorMediaInfo *info);
guint grassmoor_media_info_get_duration (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_artist (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_album (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_genre (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_year (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_album_art_url (GrassmoorMediaInfo *info);

guint grassmoor_media_info_get_height (GrassmoorMediaInfo *info);
guint grassmoor_media_info_get_width (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_date (GrassmoorMediaInfo *info);
const gchar * grassmoor_media_info_get_tag (GrassmoorMediaInfo *info);

guint grassmoor_media_info_get_number_of_elements (GrassmoorMediaInfo *info);

void grassmoor_media_info_free (GrassmoorMediaInfo *info);

G_END_DECLS

#endif //__LIBGRASSMOOR_TRACKER_H__
